//
//  DataModel.swift
//  Checklists
//
//  Created by Sachidanand on 06/02/17.
//  Copyright © 2017 Sachidanand Hiremath. All rights reserved.
//

import Foundation

class DataModel {
    var lists = [Checklist]()
    
    var indexOfSelectedChecklistItem: Int {
        get {
            return UserDefaults.standard.integer(forKey: "ChecklistIndex")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "ChecklistIndex")
            UserDefaults.standard.synchronize()
        }
    }
    
    init() {
        loadChecklists()
        registerDefaults()
        handleFirstTime()
    }
    
    func documentsDirectiry() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func dataFilePath() -> URL {
        return documentsDirectiry().appendingPathComponent("Checklists.plist")
    }
    
    func saveChecklists() {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.encode(lists, forKey: "Checklists")
        archiver.finishEncoding()
        data.write(to: dataFilePath(), atomically: true)
    }
    
    func loadChecklists() {
        let dataPath = dataFilePath()
        
        if let data = try? Data(contentsOf: dataPath) {
            let unarchiver = NSKeyedUnarchiver(forReadingWith: data)
            lists = unarchiver.decodeObject(forKey: "Checklists") as! [Checklist]
            unarchiver.finishDecoding()
            sortChecklists()
        }
        
    }
    
    func registerDefaults() {
        let defaults : [String:Any] = ["ChecklistIndex":-1, "FirstTime":true, "ChecklistItemID":0]
        UserDefaults.standard.register(defaults: defaults)
    }
    
    func handleFirstTime() {
        
        let firstTime = UserDefaults.standard.bool(forKey: "FirstTime")
        
        if firstTime {
            let firstItem = Checklist(name: "Lists")
            lists.append(firstItem)
            indexOfSelectedChecklistItem = 0
            UserDefaults.standard.set(false, forKey: "FirstTime")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    func sortChecklists() {
        lists.sort { (checklist1, checklist2) -> Bool in
            return checklist1.name.localizedCaseInsensitiveCompare(checklist2.name) == .orderedAscending
        }
    }

    class func nextChecklistItemID() -> Int {
        let userDefaults = UserDefaults.standard
        let itemID = userDefaults.integer(forKey: "ChecklistItemID")
        userDefaults.set(itemID + 1, forKey: "ChecklistItemID")
        userDefaults.synchronize()
        return itemID
    }
}
