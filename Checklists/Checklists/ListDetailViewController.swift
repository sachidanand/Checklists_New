//
//  ListDetailViewController.swift
//  Checklists
//
//  Created by Sachidanand on 05/02/17.
//  Copyright © 2017 Sachidanand Hiremath. All rights reserved.
//

import UIKit

protocol ListDetailViewControllerDelegate: class {
    func listDetailViewControllerDidCancel(_ controller: ListDetailViewController)
    func listDetailViewController(_ controller: ListDetailViewController, didFinishAdding item: Checklist)
    func listDetailViewController(_ controller: ListDetailViewController, didFinishEditing item: Checklist)
}

class ListDetailViewController: UITableViewController, UITextFieldDelegate, IconPickerViewControllerDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    weak var delegate: ListDetailViewControllerDelegate?
    var checklistToEdit: Checklist?
    @IBOutlet weak var iconImageView: UIImageView!
    
    var iconName = "Folder"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let itemToEdit = checklistToEdit {
            textField.text = itemToEdit.name
            title = "Edit Checklist"
            doneButton.isEnabled = true
            iconName = itemToEdit.iconName
            iconImageView.image = UIImage(named: iconName)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        textField.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == 1 {
            return indexPath
        } else {
            return nil
        }
    }
    
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let oldText = textField.text! as NSString
        let newText = oldText.replacingCharacters(in: range, with: string)
            as NSString
        doneButton.isEnabled = (newText.length > 0)
        return true
    }
    
    @IBAction func cancel() {
        delegate?.listDetailViewControllerDidCancel(self)
    }
    
    @IBAction func done() {
        if let checklistItem = checklistToEdit {
            checklistItem.name = textField.text!
            checklistItem.iconName = iconName
            delegate?.listDetailViewController(self, didFinishEditing: checklistItem)
        } else {
            let textEntered = textField.text!
            let newItem = Checklist(name: textEntered, iconName:iconName)
            delegate?.listDetailViewController(self, didFinishAdding: newItem)
            
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PickIcon" {
            let controller = segue.destination as! IconPickerViewController
            controller.delegate = self
        }
    }
    
    
    func iconPicker(_ picker: IconPickerViewController, didPick iconName: String) {
        self.iconName = iconName
        iconImageView.image = UIImage(named: self.iconName)
        let _ = navigationController?.popViewController(animated: true)
    }
}
