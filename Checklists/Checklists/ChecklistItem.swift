//
//  ChecklistItem.swift
//  Checklists
//
//  Created by Sachidanand on 26/01/17.
//  Copyright © 2017 Sachidanand Hiremath. All rights reserved.
//

import Foundation
import UserNotifications

class ChecklistItem : NSObject, NSCoding {
    var text: String = ""
    var checked: Bool = false
    var dueDate = Date()
    var shouldRemind = false
    var itemId: Int
    
    required init?(coder aDecoder: NSCoder) {
        text = aDecoder.decodeObject(forKey: "Text") as! String
        checked = aDecoder.decodeBool(forKey: "Checked")
        dueDate = aDecoder.decodeObject(forKey: "DueDate") as! Date
        shouldRemind = aDecoder.decodeBool(forKey: "ShouldRemind")
        itemId = aDecoder.decodeInteger(forKey: "ItemId")
        super.init()
    }
    
    init(text: String, checked: Bool) {
        self.text = text
        self.checked = checked
        itemId = DataModel.nextChecklistItemID()
        super.init()
    }
    
    override init() {
        itemId = DataModel.nextChecklistItemID()
        super.init()
    }
    
    func toggleCheckMark() {
        checked = !checked
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(text, forKey: "Text")
        aCoder.encode(checked, forKey: "Checked")
        aCoder.encode(shouldRemind, forKey: "ShouldRemind")
        aCoder.encode(itemId, forKey: "ItemId")
        aCoder.encode(dueDate, forKey: "DueDate")
    }
    
    
    func scheduleNotification() {
        removeNotifications()
        if shouldRemind && dueDate>Date() {
            let content = UNMutableNotificationContent()
            content.title = "Reminder:"
            content.body = text
            content.sound = UNNotificationSound.default()
            
            let calendar = Calendar(identifier: .gregorian)
            let components = calendar.dateComponents([.month,.day,.hour,.minute], from: dueDate)
            
            let trigger = UNCalendarNotificationTrigger(dateMatching:components , repeats: false)
            let request = UNNotificationRequest(identifier: "\(itemId)", content: content, trigger: trigger)
            let center = UNUserNotificationCenter.current()
            center.add(request)
            
            print("Scheduled Notification \(request) for itemId \(itemId)")
        }
    }
    
    func removeNotifications() {
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: ["\(itemId)"])
    }
    
    
    deinit {
        removeNotifications()
    }
    
}
