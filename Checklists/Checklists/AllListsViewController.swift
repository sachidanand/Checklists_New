//
//  AllListsViewController.swift
//  Checklists
//
//  Created by Sachidanand on 05/02/17.
//  Copyright © 2017 Sachidanand Hiremath. All rights reserved.
//

import UIKit

class AllListsViewController: UITableViewController, ListDetailViewControllerDelegate, UINavigationControllerDelegate {

    var dataModel : DataModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.delegate = self
        
        let index = dataModel.indexOfSelectedChecklistItem
        if index >= 0 && index < dataModel.lists.count {
            let checklistItem = dataModel.lists[index]
            performSegue(withIdentifier: "ShowChecklist", sender: checklistItem)
        }
    }
    
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.lists.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = makeCell()
        let listItem = dataModel.lists[indexPath.row]
        cell.textLabel?.text = listItem.name
        cell.accessoryType = .detailDisclosureButton
        let count = listItem.countUncheckedItems()
        if listItem.items.count == 0 {
            cell.detailTextLabel?.text = "No Items"
        } else if count > 0 {
            cell.detailTextLabel?.text = "\(listItem.countUncheckedItems()) Remaining"
        } else {
            cell.detailTextLabel?.text = "All Done"
        }
        
        cell.imageView!.image = UIImage(named: listItem.iconName)
        return cell
    }
    
    
    func makeCell() -> UITableViewCell {
        let cellIdentifier = "Cell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) {
            return cell
        } else {
            return UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        
    }


    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dataModel.indexOfSelectedChecklistItem = indexPath.row
        let checklistItem = dataModel.lists[indexPath.row]
        performSegue(withIdentifier: "ShowChecklist", sender: checklistItem)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowChecklist" {
            if let item = sender as? Checklist {
                let destinationVC = segue.destination as! ChecklistViewController
                destinationVC.checklist = item
            }
        } else if segue.identifier == "AddChecklist" {
            let navigationVC = segue.destination as! UINavigationController
            let destinationVC = navigationVC.topViewController as! ListDetailViewController
            destinationVC.delegate = self
        }
    }
    
    
    func listDetailViewControllerDidCancel(_ controller: ListDetailViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func listDetailViewController(_ controller: ListDetailViewController, didFinishEditing item: Checklist) {
        dataModel.sortChecklists()
        tableView.reloadData()
        dismiss(animated: true, completion: nil)
    }
    
    
    func listDetailViewController(_ controller: ListDetailViewController, didFinishAdding item: Checklist) {
        dataModel.lists.append(item)
        dataModel.sortChecklists()
        tableView.reloadData()
        dismiss(animated: true, completion: nil)
    }
    
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        dataModel.lists.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
    
    override func tableView(_ tableView: UITableView,
                            accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let navigationController = storyboard!.instantiateViewController(
            withIdentifier: "ListDetailNavigationController")
            as! UINavigationController
        let controller = navigationController.topViewController as! ListDetailViewController
        controller.delegate = self
        let checklist = dataModel.lists[indexPath.row]
        controller.checklistToEdit = checklist
        present(navigationController, animated: true, completion: nil)
    }
    
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController === self {
            dataModel.indexOfSelectedChecklistItem = -1
        }
    }
}

